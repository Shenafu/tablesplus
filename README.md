# README

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for?

* Extends BBC for SMF forums. Create tables with VSV, CSV, markdown, reStructuredText (rST), or Wiki. Can also create simple or nested lists.

## How do I get set up?

* Download TablesPlus package from SMF modifications. Install from your SMF forum's admin control panel.

The [tables=] tag must be used with one of these options:

* [tables=vsv]
* [tables=csv]
* [tables=rst]
* [tables=md]
* [tables=wiki]
* [tables=list]

## Contribution guidelines

* Writing tests
* Code review
* Other guidelines

## Who do I talk to?

* SMF modifications
