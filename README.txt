TablesPlus
mod for SMF forums
by Xay Voong
v1.0

PURPOSE: Allows BBC tag that create tables with VSV, CSV, markdown (md), reStructuredText (rST), Wiki. Can also create simple or nested lists.

COMPATIBILITY: For SMF Forums 2.0.11+

INSTRUCTIONS:

The [tables=] tag must be used with one of these options:

* [tables=vsv]
* [tables=list]
* [tables=csv]
* [tables=md]
* [tables=rst]
* [tables=wiki]

Alternatively, use the dropdown to choose a format.

* For VSV format, visit https://ieants.cc/smf/index.php?topic=203.msg2078;topicseen#msg2078 and https://forrsiis.github.io/vsv/index.html .

* On how to format tables in other formats, read http://hyperpolyglot.org/lightweight-markup .

Each table should be enclosed in their own [tables=] [/tables] tag.

Does not support cells that span multiple columns or rows.
